.PHONY: help
.DEFAULT_GOAL = help

dc = docker compose
de = $(dc) exec

# Profile
# Print output
# For colors, see https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
INTERACTIVE := $(shell tput colors 2> /dev/null)
COLOR_UP = 3
COLOR_INSTALL = 6
COLOR_WAIT = 5
COLOR_STOP = 1
PRINT_CLASSIC = cat
PRINT_PRETTY = sed 's/^/$(shell printf "\033[3$(2)m[%-7s]\033[0m " $(1))/'
PRINT_PRETTY_NO_COLORS = sed 's/^/$(shell printf "[%-7s] " $(1))/'
PRINT = PRINT_CLASSIC

.PHONY: pretty
pretty:
ifdef INTERACTIVE
	$(eval PRINT = PRINT_PRETTY)
else
	$(eval PRINT = PRINT_PRETTY_NO_COLORS)
endif
	@true

# Configuration
.PHONY: .env
.env:  ## Create dotEnv file if not exist from .dist
ifneq ("$(wildcard .env.dist)", "")
	@if [ ! -f ".env" ]; then cp .env.dist .env; fi;
endif

# INSTALL
.PHONY: start
start: up install ## Start the application

.PHONY: up
up: ## Launch docker containers
	$(dc) up -d --remove-orphans

.PHONY: install
install: ## Launch install script
	$(eval app ?= php)
	$(dc) run --rm $(app) ./bin/install

# Common
.PHONY: exec
exec: ## Get shell in container
	$(eval user ?= www)
	$(eval cmd ?= bash)
	$(eval app ?= php)
	$(de) --user $(user) $(app) $(cmd)

.PHONY: exec_redis
exec_redis:
	$(de) redis bash

.PHONY: reset
reset: clear ## Clear caches and kill containers
	$(dc) down -v --remove-orphans
	$(dc) kill
	$(dc) rm
# 	rm -rf vendor/ app/bootstrap.php.cache build-junit/
# 	find app/ -name '*.dist' -print | grep -v phpunit | rev | cut -f 2- -d '.' | rev | xargs rm -f
# 	find bin/. -maxdepth 1 -type l | xargs --no-run-if-empty file | grep "broken symbolic link to ../vendor/" | cut -d':' -f1 | xargs rm -f || true

.PHONY: ps
ps: ## Show containers
	$(dc) ps

.PHONY: logs
logs: ## Get logs from main container
	$(dc) logs -f

.PHONY: clear
clear: ## Clear cache & logs
	rm -rf var/cache/* var/log/*

# QUALITY (@todo put each in other docker images)
# .PHONY: phpstan
# phpstan:  ## phpstan
# 	vendor/bin/phpstan analyse --memory-limit=2G
#
# .PHONY: phpcs
# phpcs: ## PHP_CodeSnifer Geolid flavoured (https://github.com/Geolid/phpcs)
# 	vendor/bin/phpcs
# 	vendor/bin/php-cs-fixer fix --dry-run --diff
#
# .PHONY: phpcs-fix
# phpcs-fix: ## Automatically correct coding standard violations
# 	vendor/bin/phpcbf
# 	vendor/bin/php-cs-fixer fix
#
# .PHONY: twigcs
# twigcs: ## Twigcs (https://github.com/allocine/twigcs)
# 	vendor/bin/twigcs templates

# HELP
.PHONY: help
help: ## listing command
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
